import SiteHeader from '@components/SiteHeader/SiteHeader.vue'

import Overlay from '@components/Overlay/Overlay.vue'
export default {
	mounted() {
		this.$root.getToken()
	},
	components: { 
		SiteHeader,
		Overlay
	},
	mixins: [ 
	],
	data() {
		return {
			login: '',
			password: '',
			token: '',
			uid: ''

		};
	},
	
	methods: {
		getAuth(){
			this.$http.get( '/Auth', {
				params: {
					login: this.login,
					password: this.login,
				}
			} ).then( ( res ) => {
				this.token = res.data.token
				this.$root.uid = res.data.uid
				this.$root.auth = true
				window.localStorage.token = res.data.token
				this.$root.getToken()
			} );
		}
	
	},
	watch: {
		'$route'( from, to ) {
			
		}
	}
};
