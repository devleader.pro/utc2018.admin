export default {
	name: 'MapHere',
	mixins: [  ],
	components: {
	},
	mounted() {
		this.getDataExemple()
		this.pixelRatio = window.devicePixelRatio || 1
		this.defaultLayers = this.platform.createDefaultLayers(
			{
				tileSize: this.pixelRatio === 1 ? 256 : 512,
				ppi: this.pixelRatio === 1 ? undefined : 320
			}
		)
		this.map = new H.Map(
			this.$refs.map,
			this.defaultLayers.normal.map,
			{
				zoom: 10,
				center: { lng: this.lng, lat: this.lat },
				pixelRatio: this.pixelRatio,
			}
		);
		this.behavior = new H.mapevents.Behavior(new H.mapevents.MapEvents(this.map))
		this.ui = H.ui.UI.createDefault(this.map, this.defaultLayers, 'ru-RU');
	},
	props: {
		appId: String,
		appCode: String,
		lat: String,
		lng: String,
		width: String,
		height: String
	},
	created() {
		this.platform = new H.service.Platform({
			"app_id": this.appId,
			"app_code": this.appCode
		});
	},
	data: function() {
		return {
			map: {},
			ui: {},
			platform: {},
			defaultLayers: {},
			pixelRatio: 0,
			behavior: {},
			mapEvents: {},
			data: [],
			dataReady: false,
			icon: {},
			markers: [],
		};
	},
	methods: {
		
		getDataExemple(){
			this.$http.get( '/HeatMap', {headers: {
				Token: this.$root.token,
			}}).then( ( res ) => {
				
				this.data = res.data;
				for (let index = 0; index < res.data.length; index++) {
					
					const el = res.data[index];
					let icon = new H.map.Icon(`<svg xmlns="http://www.w3.org/2000/svg" width="${86 + el.defectIdsList.length*5}" height="${86 + el.defectIdsList.length*5}" viewBox="0 0 86 86">
						<g fill="none" fill-rule="evenodd">
							<circle cx="43" cy="43" r="42.197" fill="#B50D10" opacity=".3"/>
							<circle cx="43" cy="43" r="33.271" fill="#B50D10" opacity=".3"/>
							<circle cx="43" cy="43" r="22.722" fill="#D51317"/>
							<text fill="#FFF" font-family="'Roboto', sans-serif" font-size="19" text-anchor="center">
								<tspan width="100" x="38" y="49" >
									${el.defectIdsList.length}
								</tspan>
							</text>
						</g>
					</svg>`)
					let coords = {lat: el.lat, lng: el.lon};
					let marker = new H.map.Marker(coords, {icon: icon});
					this.map.addObject(marker)
				}
				this.dataReady = true
			} );

		},
	
	},
	watch: {
		'$route'( to, from ) {
		}
	}
};
