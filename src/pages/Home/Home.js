import SitePanel from '@components/SitePanel/SitePanel.vue'

export default {
	name: 'home',
	mixins: [ 

	],
	components: {
		SitePanel
	},
	mounted() {
		this.getData()
		this.getUsers()
	},
	props: {
		
	},
	data: function() {
		return {
			dataAll: [],
			data: [],
			dataReady: false,
			users: [],
			usersReady: false,
			countOk: 0,
			countErrors: 0,
			file:'',
			image: ''

		};
	},
	methods: {
		getName(sn,fn,pn){
			const f = fn.split('')[0];
			const p = pn.split('')[0];
			return `${sn}\xa0${f}.${p}.`

		},
		getTime(time){
			const date = new Date(time);
			const day = (date.getDate() < 10 ) ? '0' + date.getDate() : '' + date.getDate() 
			const month = (date.getMonth() < 9 ) ? '0' + ( date.getMonth() + 1 ) : '' + ( date.getMonth() + 1 ) 
			return `${day}.${month}.${date.getFullYear()}`
		},
		getUsers(){
			this.$http.get( '/User').then( ( res ) => {
				this.users = res.data;
				this.usersReady = true;
			} );
		},
		getData(){
			this.$http.get( '/Task', {headers: {
				Token: this.$root.token,
			}}).then( ( res ) => {
				this.dataAll = res.data;
				this.count();
				this.getAll();
			} );

		},
		getAll(){
			this.data = this.dataAll;
			this.dataReady = true;

		},
		count(){
			for (let index = 0; index < this.dataAll.length; index++) {
				const el = this.dataAll[index];
				if(el.taskStatusId === 4){
					this.countOk = this.countOk + 1
				} 
			};
			this.$http.get( '/HeatMap', {headers: {
				Token: this.$root.token,
			}}).then( ( res ) => {
				for (let index = 0; index < res.data.length; index++) {
					const el = res.data[index];
					this.countErrors = this.countErrors + el.defectIdsList.length
				}
			} );
	
		},
		getNumerals( number, titles ) {  
			const cases = [ 2, 0, 1, 1, 1, 2 ];  
			return titles[ ( number % 100 > 4 && number % 100 < 20 ) ? 2 : cases[ ( number % 10 < 5 ) ? number % 10 : 5 ] ];  
		},
			
		fileChange( e ) {
			const files = e.target.files || e.dataTransfer.files;
			if ( ! files.length )
				return;
			this.createImage( files[ 0 ]);
		},
		createImage( file ) {
			this.file = file;
			// const image = new Image();
			const reader = new FileReader();
			
			reader.onload = ( e ) => {
				this.image = e.target.result;
			};
			reader.readAsDataURL( file );
			console.log(file)
			// this.addImages();
		},
		
		addFile() {
			this.readyToPost = 'false'
			this.$http.post(  '/Customer/UploadFiles', this.file,
				{
					headers: {
						'Content-Disposition': `attachment; filename="${ this.file.name }"`,
						'Content-Type': this.file.type,
						'Access-Control-Allow-Origin': '*'
					}
				}
			).then( ( res ) => {
				this.imageId = res.data.id;
				this.readyToPost = 'true';
			} ).catch((e) => {
			})
		},
		
		
	},
	watch: {
		'$route'( to, from ) {
		}
	}
};
