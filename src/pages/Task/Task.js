import SitePanel from '@components/SitePanel/SitePanel.vue'
import MapHere from '@components/MapHere2/MapHere.vue'
export default {
	name: 'page',
	mixins: [  ],
	components: {
		SitePanel,MapHere
	},
	mounted() {
		this.getTaskId()

	},
	props: {
		
	},
	data: function() {
		return {
			taskId: 0,
			data: [],
			dataReady: false,
			
			
		};
	},
	methods: {
		print(){
			window.print()
		},
		getTaskId(){
			this.taskId = this.$route.params.id;
			this.getDataExemple()
		},
		getDataExemple(){
			this.$http.get( '/Task/'+this.taskId, {headers: {
				Token: this.$root.token,
			}}).then( ( res ) => {
				this.data = res.data;
				this.dataReady = true;
			} )
			
		},
		
		getName(sn,fn,pn){
			const f = fn.split('')[0];
			const p = pn.split('')[0];
			return `${sn}\xa0${f}.${p}.`

		},
		getTime(time){
			const date = new Date(time);
			const day = (date.getDate() < 10 ) ? '0' + date.getDate() : '' + date.getDate() 
			const month = (date.getMonth() < 9 ) ? '0' + ( date.getMonth() + 1 ) : '' + ( date.getMonth() + 1 ) 
			return `${day}.${month}.${date.getFullYear()}`
		},
	
	},
	watch: {
		'$route'( to, from ) {
			this.getTaskId()
		}
	}
};
