export default {
	name: 'page',
	mixins: [  ],
	components: {
		
	},
	mounted() {
		
	},
	props: {
		title: String,
		required: true
	},
	data: function() {
		return {
			data: [],
			dataReady: false
		};
	},
	methods: {
		
	
	},
	watch: {
		'$route'( to, from ) {
		}
	}
};
