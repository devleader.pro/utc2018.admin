import SitePanel from '@components/SitePanel/SitePanel.vue'

export default {
	name: 'page',
	mixins: [  ],
	components: {
		SitePanel,
	},
	mounted() {
		this.getUserId()

	},
	props: {
		
	},
	data: function() {
		return {
			userId: 0,
			data: [],
			dataReady: false,
			tasksDaya: [],
			tasksReady: false,
			countAll: 0,
			countOk: 0,
		};
	},
	methods: {
		getUserId(){
			this.userId = this.$route.params.id;
			this.getDataExemple()
		},
		getDataExemple(){
			this.$http.get( '/User/'+this.userId, {headers: {
				Token: this.$root.token,
			}}).then( ( res ) => {
				this.data = res.data;
				this.dataReady = true;
			} )
			this.$http.get( '/Task/', {headers: {
				Token: this.$root.token,
			}}).then( ( res ) => {
				this.tasksDaya = res.data;
				this.tasksReady = true;
				for (let index = 0; index < res.data.length; index++) {
					const el = res.data[index];
					if(el.userId == this.userId){
						this.countAll++;
						if(el.taskStatusId === 4){
							this.countOk++
						}
					}
				}
			} )
		},
		
		getName(sn,fn,pn){
			const f = fn.split('')[0];
			const p = pn.split('')[0];
			return `${sn}\xa0${f}.${p}.`

		},
		getTime(time){
			const date = new Date(time);
			const day = (date.getDate() < 10 ) ? '0' + date.getDate() : '' + date.getDate() 
			const month = (date.getMonth() < 9 ) ? '0' + ( date.getMonth() + 1 ) : '' + ( date.getMonth() + 1 ) 
			return `${day}.${month}.${date.getFullYear()}`
		},
	
	},
	watch: {
		'$route'( to, from ) {
			this.getUserId()
		}
	}
};
