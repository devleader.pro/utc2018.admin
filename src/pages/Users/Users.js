import SitePanel from '@components/SitePanel/SitePanel.vue'

export default {
	name: 'page',
	mixins: [  ],
	components: {
		SitePanel,
	},
	mounted() {
		this.getDataExemple()
	},
	props: {
		
	},
	data: function() {
		return {
			data: [],
			dataReady: false,
			alltasks: []
		};
	},
	methods: {
		getDataExemple(){
			this.$http.get( '/User', {headers: {
				Token: this.$root.token,
			}}).then( ( res ) => {
				this.data = res.data;
				this.dataReady = true;
			} )
			this.$http.get( '/Task/', {headers: {
				Token: this.$root.token,
			}}).then( ( res ) => {

				this.alltasks = res.data
				
			} )

		},
		getStat(uid){
			let countOk = 0, count = 0
			for (let index = 0; index < this.alltasks.length; index++) {
				const el = this.alltasks[index];
				if(el.userId == uid){
					count++;
					if(el.taskStatusId === 4){
						countOk++
					}
				}
			}
			return `${countOk}/${count}`
		}
	
	},
	watch: {
		'$route'( to, from ) {
		}
	}
};
