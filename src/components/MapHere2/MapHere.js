export default {
	name: 'MapHere',
	mixins: [  ],
	components: {
	},
	mounted() {
		this.getDataExemple()
		this.pixelRatio = window.devicePixelRatio || 1
		this.defaultLayers = this.platform.createDefaultLayers(
			{
				tileSize: this.pixelRatio === 1 ? 256 : 512,
				ppi: this.pixelRatio === 1 ? undefined : 320
			}
		)
		this.map = new H.Map(
			this.$refs.map,
			this.defaultLayers.normal.map,
			{
				zoom: 15,
				center: { lng: this.lng, lat: this.lat },
				pixelRatio: this.pixelRatio,
			}
		);
		this.behavior = new H.mapevents.Behavior(new H.mapevents.MapEvents(this.map))
		this.ui = H.ui.UI.createDefault(this.map, this.defaultLayers, 'ru-RU');
		let icon = new H.map.Icon(`<svg xmlns="http://www.w3.org/2000/svg" width="50" height="50" viewBox="0 0 86 86" style="margin-bottom: -25px;">
				<g fill="none" fill-rule="evenodd">
					<circle cx="43" cy="43" r="43" fill="#D51317"/>
					
				</g>
			</svg>`)
			let coords = {lat: this.lat, lng: this.lng};
			let marker = new H.map.Marker(coords, {icon: icon});
			this.map.addObject(marker)
	},
	props: {
		appId: String,
		appCode: String,
		lat: String,
		lng: String,
		width: String,
		height: String
	},
	created() {
		this.platform = new H.service.Platform({
			"app_id": this.appId,
			"app_code": this.appCode
		});
	},
	data: function() {
		return {
			map: {},
			ui: {},
			platform: {},
			defaultLayers: {},
			pixelRatio: 0,
			behavior: {},
			mapEvents: {},
			data: [],
			dataReady: false,
			icon: {},
			markers: [],
		};
	},
	methods: {
		
		getDataExemple(){
			
		},
	
	},
	watch: {
		'$route'( to, from ) {
		}
	}
};
