//import Plugins
import "babel-polyfill";
import Vue from 'vue';
import VueRouter from 'vue-router';
import Vuex from 'vuex';
import { HTTP as axios } from './axios/axios';
Vue.prototype.$http = axios;
// register global

Vue.use( Vuex );
Vue.use( VueRouter );
Vue.config.debug = true;
Vue.config.devTools = true;

import App from './App.vue';
Vue.component( 'App', App );
import Users from '@pages/Users/Users.vue';
Vue.component( 'Users', Users );
import MapPage from '@pages/MapPage/MapPage.vue';
Vue.component( 'MapPage', MapPage );
import Home from '@pages/Home/Home.vue';
Vue.component( 'Home', Home );
import Profile from '@pages/Profile/Profile.vue';
Vue.component( 'Profile', Profile );
import Task from '@pages/Task/Task.vue';
Vue.component( 'Task', Task );
//Routers
const router = new VueRouter( {
	mode: 'history',
	routes: [
		
		{ path: '/today', name: 'Home', component: Home }, 	
		{ path: '/today/:id', name: 'Task', component: Task }, 	
		{ path: '/map', name: 'MapPage', component: MapPage }, 	
		{ path: '/', redirect: '/today' }, 	
		{ path: '/users', component: Users },
		{ path: '/users/:id', component: Profile },
		
	]
} );

const store = new Vuex.Store( {
	state: {
		title: ''
	},
	mutations: {
		rtChangeTitle( state, value ) {
			state.title = value;
			document.title = ( state.title ? state.title + ' - ' : '' ) + 'Личный кабинет администратора ГКУ';
		}
	}
} );

new Vue( {
	router,
	store,
	render: createElement => createElement( App ),
	data() {
		return {
			auth: false,
			token: '',
			uid: '',
			me: {},
			meReady:true
		};
	},
	methods: {
		getUid(){
			this.token = window.localStorage.token
			this.auth = true;
		},
		getToken(){
			if(window.localStorage.token){
				this.$http.get(`User/uid/${window.localStorage.token}`).then((res)=>{
					this.getUid();
					this.uid = res.data.id
					this.me = res.data
				}).catch((e) => {
					window.localStorage.token = undefined
					this.token = ''
					this.auth = false
					this.uid = null
				})
				
			} else {
				window.localStorage.token = undefined
				this.token = ''
				this.auth = false
				this.uid = null
			}
		}
	}
} ).$mount( '#app' );
