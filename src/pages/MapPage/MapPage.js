import SitePanel from '@components/SitePanel/SitePanel.vue'
import MapHere from '@components/MapHere/MapHere.vue'

export default {
	name: 'page',
	mixins: [  ],
	components: {
		SitePanel,
		MapHere
	},
	mounted() {
		this.getDataExemple()
	},
	props: {
		
	},
	data: function() {
		return {
			data: [],
			dataReady: false,
		};
	},
	methods: {
		getDataExemple(){
			this.$http.get( '/HeatMap', {headers: {
				Token: this.$root.token,
			}}).then( ( res ) => {
				this.data = res.data;
				this.dataReady = true;
			} );

		}
	
	},
	watch: {
		'$route'( to, from ) {
		}
	}
};
